{-# LANGUAGE FlexibleContexts #-}

module Database.Relational.Query.SQLite3 (
    module Database.HDBC.Sqlite3
  , makeRelationalRecord
  ) where

import Language.Haskell.TH (Name, Q, Dec)
import Database.HDBC.Query.TH hiding (makeRelationalRecord)
import Database.HDBC.Sqlite3
import Database.Hareru.Wrapper

makeRelationalRecord :: Name -> Q [Dec]
makeRelationalRecord = makeRelationalRecord' defaultConfig
